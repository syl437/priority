import {Component, OnInit} from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import {ServerService} from "../../app/services/server-service";

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage implements OnInit {

  public jsonResponse : any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public server:ServerService) {
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');

  }

    async ngOnInit() {



        this.server.GetData('https://aviatest.wee.co.il/odata/Priority/tabula.ini/avia/PRIT_LOADDOC').then((data: any) => {
          alert (data);
            console.log("getPastClasses : " , data.json());
            this.jsonResponse = data.json();
            //let response = data.json();
            //this.ClassesArray = response;
            //console.log("ClassesArray",this.ClassesArray)
        });


    }

}
